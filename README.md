# DjangoGirls Manchester 2017 National Coding Week 

## Create and Load Virtual Environment

### Conda

~~~
$ conda create -n DjangoGirlsManchester2017NationalCodingWeek python=3.6
$ source activate DjangoGirlsManchester2017NationalCodingWeek
~~~

### VirtualEnv

~~~
$ pythom -m venv create .
$ source bin/activate
~~~

## Install Packages

~~~
$ python -m pip install -r requirements.txt
~~~

## How to Create Project and App

**The output of this section was already saved to Git.**

~~~
$ django-admin startproject mysite
$ python manage.py startapp survey
~~~

E addition, the output of

~~~
$ python manage.py makemigrations
~~~

is also saved to Git.

## Create Database

~~~
$ python manage.py migrate
~~~

## Populate Database

We will make use of [django-autofixture](https://github.com/gregmuellegger/django-autofixture)
to populate the database.

~~~
$ python manage.py loadtestdata survey.Answer:1000 -u survey.autofixtures.AnswerFixture
~~~

## Data Analyse

Run

~~~
$ python manage.py shell_plus --notebook
~~~

and follow the content of `data-analyse.ipynb`.
