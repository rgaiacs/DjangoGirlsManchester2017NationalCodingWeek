import autofixture

from .models import FIVE_POINT_LIKERT_SCALE

class AnswerFixture(autofixture.AutoFixture):
    class Values:
        is_django_girls_the_best_group = autofixture.generators.ChoicesGenerator(
            values=[key for (key, description) in FIVE_POINT_LIKERT_SCALE]
        )
