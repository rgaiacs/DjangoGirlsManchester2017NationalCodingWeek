from django.db import models

FIVE_POINT_LIKERT_SCALE = (
    ('1', 'Strongly Disagree'),
    ('2', 'Disagree'),
    ('3', 'Neutral'),
    ('4', 'Agree'),
    ('5', 'Strongly Agree'),
)

class Answer(models.Model):
    """Describe a answer."""

    is_django_girls_the_best_group = models.CharField(
        choices=FIVE_POINT_LIKERT_SCALE,
        max_length=1,
        default="3"
    )
